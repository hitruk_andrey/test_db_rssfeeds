import pytest
import yaml
import mysql.connector
from mysql.connector import errorcode

@pytest.fixture
def connect_db():
    try:
        config = yaml.safe_load(open('tests/config.yml'))
        cnx = mysql.connector.connect(**config)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        cursor = cnx.cursor()
        yield cursor
        cnx.close()