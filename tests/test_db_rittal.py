pytest_plugins = ['fixture_db']

def test_csv_double_apostrophe(csv_list_rittal):

    """Двойные кавычки в артикулах rittal csv"""
    double_apostrophe_csv = []  # список ean2 столбца csv
    for row in csv_list_rittal:
        if "'" in row[0]:
            double_apostrophe_csv.append(row[0])
    assert double_apostrophe_csv == [], print(double_apostrophe_csv)


def test_double_quotes_rit(connect_db):

    """Двойные кавычки в артикулах rittal db"""
    sql = "SELECT ean2 FROM rssfeeds WHERE rital = 1"
    connect_db.execute(sql)
    error_ean2 = []  # список позиций с двойными кавычками
    for row in connect_db:
        if "'" in row:
            error_ean2.append(row)
    assert error_ean2 == [], print(error_ean2)


def test_compare_attribute_ean2(connect_db, csv_list_rittal):

    """Все CSV артикулы совпадает c db ean2 rittal"""
    ean2_csv = []  # список ean2 столбца csv
    for row in csv_list_rittal:
        ean2_csv.append(row[0])

    ean2_db = []  # список ean2 артикулов db
    sql = "SELECT ean2 FROM rssfeeds WHERE rital = 1"
    connect_db.execute(sql)
    for row in connect_db:
        if row not in ean2_db:
            ean2_db.append(row[0])

    result = list(set(ean2_csv) - set(ean2_db))
    assert result == [], print(result)


def test_price(connect_db, csv_list_rittal):

    """Совпадение цен в csv, db"""
    db_ean2_price = []  # список db ean2, price
    sql = "SELECT ean2, price FROM rssfeeds WHERE rital = 1"
    connect_db.execute(sql)
    for row in connect_db:
        if row not in db_ean2_price:
            db_ean2_price.append(list(row))

    csv_ean2_price = []  # список артикулов ean2, price в csv
    for row in csv_list_rittal:
        row[6] = row[6].replace(".", ",")
        csv_ean2_price.append([row[0], row[6]])

    errore_list = []  # список несовпавших значений ean2, price, со значениями в db
    for row in csv_ean2_price:
        if row not in db_ean2_price:
           errore_list.append(row)
    assert errore_list == [], print(errore_list)


def test_db_price_null(connect_db, csv_list_rittal):

    """Список позиций, где должна быть цена == 0 """
    sql = "SELECT ean2, price FROM rssfeeds WHERE rital = 1"
    connect_db.execute(sql)

    db_ean2_price = []  # список db ean2, price
    for row in connect_db:
        if row not in db_ean2_price:
            db_ean2_price.append(list(row))

    ean2_csv = []  # список csv ean2 csv
    for row in csv_list_rittal:
        ean2_csv.append(row[0])

    db_price_null = []  # список db ean2, которых нет в csv
    for row in db_ean2_price:
        if row[0] not in ean2_csv:
            db_price_null.append(row[0])

    price_not_null = []  # список db ean2, price != 0
    for row in db_price_null:
        if row[1] != 0:
            price_not_null.append(row)
    assert price_not_null == [], print(len(price_not_null))