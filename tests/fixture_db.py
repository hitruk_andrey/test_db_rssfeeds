import pytest
import csv
import mysql.connector
from mysql.connector import errorcode
import yaml


@pytest.fixture
def connect_db():
    try:
        config = yaml.safe_load(open('tests/config.yml'))
        cnx = mysql.connector.connect(**config)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        cursor = cnx.cursor()
        yield cursor
        cnx.close()

@pytest.fixture
def csv_list_rittal():
    with open('files/rittal_2019.csv', 'r', newline='', encoding='utf-8') as file_in:
        reader = csv.reader(file_in, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        rittal_list_in = list(reader)
    return rittal_list_in

